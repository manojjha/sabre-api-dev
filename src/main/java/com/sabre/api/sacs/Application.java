package com.sabre.api.sacs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import com.sabre.api.sacs.config.ConfigurationConfig;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Module configuration an main class for running test flow.
 */
@SpringBootApplication
@ComponentScan
@EnableWebMvc
@EnableAutoConfiguration
public class Application {

	public ApplicationContext getApplicationContext(String... args) throws Exception {
		return SpringApplication.run(new Object[] { ConfigurationConfig.class, Application.class }, args);
	}

	public static void main(String[] args) {
		SpringApplication.run(new Object[] { ConfigurationConfig.class, Application.class }, args);
	}

}
